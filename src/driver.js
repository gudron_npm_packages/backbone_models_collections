// Backbone.Galament
// ---------

import Backbone from 'backbone';

// Models
import CommonModel from './models/commonModel';
import CommonPrivateModel from './models/commonPrivateModel';

// Collections
import CommonCollection from './collections/commonCollection';
import CommonPrivateCollection from './collections/commonPrivateCollection';

import {version} from '../version.json';


let BackboneGalament = {
    version: version
};

BackboneGalament.CommonModel = CommonModel;
BackboneGalament.CommonPrivateModel = CommonPrivateModel;

BackboneGalament.CommonCollection = CommonCollection;
BackboneGalament.CommonPrivateCollection = CommonPrivateCollection;

Backbone.Galament = BackboneGalament;

export default BackboneGalament;