(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("backbone"), require("underscore"), require("moment"));
	else if(typeof define === 'function' && define.amd)
		define(["backbone", "underscore", "moment"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("backbone"), require("underscore"), require("moment")) : factory(root["backbone"], root["underscore"], root["moment"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_2__, __WEBPACK_EXTERNAL_MODULE_7__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("backbone");

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; // Backbone.Galament.CommonModel
// ---------

var _underscore = __webpack_require__(2);

var _underscore2 = _interopRequireDefault(_underscore);

var _backbone = __webpack_require__(0);

var _backbone2 = _interopRequireDefault(_backbone);

var _moment = __webpack_require__(7);

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CommonModel = _backbone2.default.Model.extend({
    emulateJSON: true,
    _accessors: {},
    _mutators: {},
    _casts: {},
    _dateFormat: 'YYYY-MM-DD HH:mm:ss',
    url: function url() {
        var Config = App.getOption('config');

        var base = Config.apiUrl + '/' + Config.apiVersion + _underscore2.default.result(this, 'urlRoot');

        if (this.isNew()) {
            return base;
        }
        var id = this.attributes[this.idAttribute];

        return base.replace(/[^\/]$/, '$&/') + encodeURIComponent(id);
    },
    save: function save(attributes, options) {

        var that = this;

        options = options || {};

        var success = options.success;
        options.success = function (model, response, opt) {
            that.trigger('save:success', model, response, opt);
            if (success) {
                success.call(options.context, model, response, opt);
            }
        };

        var error = options.error;
        options.error = function (model, response, opt) {
            that.trigger('save:error', model, response, opt);
            if (error) {
                error.call(options.context, model, response, opt);
            }
        };

        var result = _backbone2.default.Model.prototype.save.call(this, attributes, options);

        return result;
    },
    fetch: function fetch(options) {

        var that = this;

        options = options || {};

        options.data = options.data || {};
        options.data = _underscore2.default.extend({
            forceIdReturn: 1
        }, options.data);

        var success = options.success;
        options.success = function (model, response, opt) {
            that.trigger('fetch:success', model, response, opt);
            if (success) {
                success.call(options.context, model, response, opt);
            }
        };

        var error = options.error;
        options.error = function (model, response, opt) {
            that.trigger('fetch:error', model, response, opt);
            if (error) {
                error.call(options.context, model, response, opt);
            }
        };

        var result = _backbone2.default.Model.prototype.fetch.call(this, options);

        return result;
    },
    destroy: function destroy(options) {
        var that = this;

        options = options || {};

        var success = options.success;
        options.success = function (model, response, opt) {
            model.trigger('destroy:success', model, response, opt);
            if (success) {
                success.call(options.context, model, response, opt);
            }
        };

        var error = options.error;
        options.error = function (model, response, opt) {
            model.trigger('destroy:error');
            if (error) {
                error.call(options.context, response, opt);
            }
        };

        var result = _backbone2.default.Model.prototype.destroy.call(this, options);

        return result;
    },
    set: function set(key, val, options) {
        var attrs;
        var result = {};

        if ((typeof key === 'undefined' ? 'undefined' : _typeof(key)) === 'object') {
            attrs = key;
            options = val;
        } else {
            (attrs = {})[key] = val;
        }

        // For each `set` attribute, update or delete the current value.
        for (var attr in attrs) {
            val = attrs[attr];

            if (this._hasSetMutator(attr)) {
                val = this._mutators[attr].call(this, attrs[attr]);
            }

            if (this._hasCast(attr)) {

                if (!_underscore2.default.isNull(attrs[attr])) {

                    switch (this._getCastType(attr)) {
                        case 'int':
                        case 'integer':
                            val = parseInt(attrs[attr]);
                            break;
                        case 'bool':
                        case 'boolean':
                            val = _underscore2.default.isBoolean(attrs[attr]) ? +attrs[attr] : parseInt(attrs[attr]);
                            break;
                        case 'date':
                        case 'datetime':
                            val = this._fromDate(attrs[attr]);
                            break;
                        default:
                            break;
                    }
                }
            }

            result[attr] = val;
        }

        return _backbone2.default.Model.prototype.set.call(this, result, options);
    },
    _hasSetMutator: function _hasSetMutator(attr) {
        return !_underscore2.default.isUndefined(this._mutators[attr]);
    },
    // Get the value of an attribute.
    get: function get(attr) {
        var val = _underscore2.default.get(this.attributes, attr);

        if (this._hasGetMutator(attr)) {
            val = this._mutateAttribute(attr, val);
        }

        if (this._hasCast(attr)) {
            val = this._castAttribute(attr, val);
        }

        return val;
    },
    _hasGetMutator: function _hasGetMutator(attr) {
        return !_underscore2.default.isUndefined(this._accessors[attr]);
    },
    _mutateAttribute: function _mutateAttribute(attrName, value) {
        return this._accessors[attrName].call(this, value);
    },
    _hasCast: function _hasCast(attr) {
        return !_underscore2.default.isUndefined(this._casts[attr]);
    },
    _getCastType: function _getCastType(attr) {
        return this._casts[attr];
    },
    _castAttribute: function _castAttribute(attr, value) {
        if (_underscore2.default.isNull(value)) {
            return value;
        }

        var val = _underscore2.default.get(this.attributes, attr);

        switch (this._getCastType(attr)) {
            case 'int':
            case 'integer':
                return parseInt(val);
            case 'string':
                return val.toString();
            case 'bool':
            case 'boolean':
                return !!+val;
            case 'date':
            case 'datetime':
                return this._asDate(val);
            default:
                return value;

        }
    },
    _fromDate: function _fromDate(value) {
        var format = this._dateFormat;

        var momentValue = this._asDate(value);

        return momentValue.format(format);
    },
    _asDate: function _asDate(value) {
        if (_moment2.default.isMoment(value)) {
            return value;
        }

        if (value instanceof Date) {
            return (0, _moment2.default)(value);
        }

        if (_underscore2.default.isNumber(value) && !_underscore2.default.isNaN(value)) {
            return _moment2.default.unix(value);
        }

        if (/^(\d{4})-(\d{2})-(\d{2})$/.test(value)) {
            return (0, _moment2.default)(value, 'YYYY-MM-DD');
        }

        if (/^(\d{4}).(\d{2}).(\d{2})$/.test(value)) {
            return (0, _moment2.default)(value, 'YYYY.MM.DD');
        }

        return (0, _moment2.default)(value, this._dateFormat);
    }

});

exports.default = CommonModel;

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("underscore");

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _commonModel = __webpack_require__(1);

var _commonModel2 = _interopRequireDefault(_commonModel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CommonPrivateModel = _commonModel2.default.extend({
    sync: function sync(method, model, options) {
        var that = this;

        options = options || {};

        var beforeSend = options.beforeSend;
        options.beforeSend = function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + App.apiToken.get('access_token'));

            if (beforeSend) {
                beforeSend.call(options.context, xhr);
            }
        };

        var result = _commonModel2.default.prototype.sync.call(this, method, model, options);

        return result;
    }
}); // Backbone.Galament.CommonPrivateModel
// ---------

exports.default = CommonPrivateModel;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _underscore = __webpack_require__(2);

var _underscore2 = _interopRequireDefault(_underscore);

var _backbone = __webpack_require__(0);

var _backbone2 = _interopRequireDefault(_backbone);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Backbone.Galament.CommonCollection
// ---------

var CommonCollection = _backbone2.default.Collection.extend({
    url: function url() {
        var Config = App.getOption('config');

        var base = Config.apiUrl + '/' + Config.apiVersion + _underscore2.default.result(this, 'urlRoot');

        return base;
    },
    save: function save(attributes, options) {

        var collection = this;

        options = options || {};

        var success = options.success;
        options.success = function (resp) {
            collection.trigger('save:success', resp);
            if (success) {
                success.call(options.context, collection, resp, options);
            }
        };

        var error = options.error;
        options.error = function (resp) {
            collection.trigger('save:error');
            if (error) {
                error.call(options.context, collection, resp, options);
            }
        };

        var result = _backbone2.default.Collection.prototype.save.call(this, attributes, options);

        return result;
    },
    fetch: function fetch(options) {

        var that = this;

        options = options || {};

        options.data = options.data || {};
        options.data = _underscore2.default.extend({
            forceIdReturn: 1,
            _disablePagination: 0
        }, options.data);

        var success = options.success;
        options.success = function (resp) {
            that.trigger('fetch:success', resp);
            if (success) {
                success.call(options.context, resp);
            }
        };

        var error = options.error;
        options.error = function (resp) {
            that.trigger('fetch:error');
            if (error) {
                error.call(options.context, resp);
            }
        };

        var result = _backbone2.default.Collection.prototype.fetch.call(this, options);

        return result;
    },
    destroy: function destroy(options) {
        var that = this;

        options = options || {};

        var success = options.success;
        options.success = function (resp) {
            that.trigger('destroy:success', resp);
            if (success) {
                success.call(options.context, resp);
            }
        };

        var error = options.error;
        options.error = function (resp) {
            that.trigger('destroy:error');
            if (error) {
                error.call(options.context, resp);
            }
        };

        var result = _backbone2.default.Collection.prototype.destroy.call(this, options);

        return result;
    }
});

exports.default = CommonCollection;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(6);


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(0);

var _backbone2 = _interopRequireDefault(_backbone);

var _commonModel = __webpack_require__(1);

var _commonModel2 = _interopRequireDefault(_commonModel);

var _commonPrivateModel = __webpack_require__(3);

var _commonPrivateModel2 = _interopRequireDefault(_commonPrivateModel);

var _commonCollection = __webpack_require__(4);

var _commonCollection2 = _interopRequireDefault(_commonCollection);

var _commonPrivateCollection = __webpack_require__(8);

var _commonPrivateCollection2 = _interopRequireDefault(_commonPrivateCollection);

var _version = __webpack_require__(9);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Backbone.Galament
// ---------

var BackboneGalament = {
    version: _version.version
};

// Collections


// Models


BackboneGalament.CommonModel = _commonModel2.default;
BackboneGalament.CommonPrivateModel = _commonPrivateModel2.default;

BackboneGalament.CommonCollection = _commonCollection2.default;
BackboneGalament.CommonPrivateCollection = _commonPrivateCollection2.default;

_backbone2.default.Galament = BackboneGalament;

exports.default = BackboneGalament;

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _commonCollection = __webpack_require__(4);

var _commonCollection2 = _interopRequireDefault(_commonCollection);

var _commonPrivateModel = __webpack_require__(3);

var _commonPrivateModel2 = _interopRequireDefault(_commonPrivateModel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _commonCollection2.default.extend({
    model: _commonPrivateModel2.default,
    sync: function sync(method, model, options) {
        var that = this;

        options = options || {};

        var beforeSend = options.beforeSend;

        options.beforeSend = function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + App.apiToken.get('access_token'));

            if (beforeSend) {
                beforeSend.call(options.context, xhr);
            }
        };

        var result = _commonCollection2.default.prototype.sync.call(this, method, model, options);

        return result;
    }
});

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = {"version":"0.0.2","buildDate":"2017-12-21"}

/***/ })
/******/ ]);
});
//# sourceMappingURL=backbone.galament.js.map